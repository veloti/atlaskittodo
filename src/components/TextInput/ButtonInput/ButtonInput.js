import React from "react";
import Button from '@atlaskit/button';
import classes from './ButtonInput.module.css'

const ButtonInput = props => {
    return (
            <Button
                appearance="primary"
                onClick={props.onSendHandler}
            >Отправить</Button>
    )
}

export default ButtonInput