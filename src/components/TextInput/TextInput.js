import React from "react";
import Textfield from '@atlaskit/textfield';
import ButtonInput from "./ButtonInput/ButtonInput";
import classes from './TextInput.module.css'

const TextInput = props => {
    return (
        <div className={classes.TextInput}>
            <Textfield
                onChange={props.onChange}
            />
            <ButtonInput
                onSendHandler={props.onSendHandler}
            />
        </div>
    )
}

export default TextInput