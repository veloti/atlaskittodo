import React from "react";
import classes from './Task.module.css';
import CrossIcon from '@atlaskit/icon/glyph/cross';

const Task = props => {
    return (
        <div className={classes.Task}>
            <div className={classes.TaskText}>
                {props.text}
            </div>
            <div className={classes.TaskIcon}
                onClick={props.onDelete}

            >
                <CrossIcon/>
            </div>
        </div>
    )
}

export default Task