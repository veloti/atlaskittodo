import React,{Component} from 'react';
import './App.css';
import Button from '@atlaskit/button';
import Task from './components/Task/Task';
import TextInput from "./components/TextInput/TextInput";

class App extends Component {
    state = {
        tasks: ['Do the homework','Clean up the room','Wash up'],
        inputState: ''
    }

    onChangeHandler(text) {
        this.setState({inputState: text})
        console.log(this.state.inputState)
    }

    onSendHandler() {
        const tasks = this.state.tasks;
        const inputState = this.state.inputState;
        tasks.push(inputState);
        this.setState({tasks: tasks})
    }

    deleteHandler(index) {
        const tasks = this.state.tasks;
        tasks.splice(index,1);
        this.setState({tasks})
    }

    render() {
        let tasks = null;

        tasks = this.state.tasks.map((task, index) => {
            return (
                <Task
                    text={task}
                    key={index}
                    onDelete={this.deleteHandler.bind(this, index)}
                />
            )
        });

        return (

            <div className="App">
                <div className="App__wrapper">
                    <div>{tasks}</div>
                    <TextInput
                    onChange={event => this.onChangeHandler(event.target.value)}
                    onSendHandler={this.onSendHandler.bind(this)}
                    />
                </div>
            </div>
        )
    }
}

export default App;
